import app from './App';

const PORT = (process.env.PORT) ? process.env.PORT : 8000;
const SERVER = (process.env.SERVER) ? process.env.SERVER : 'localhost';

(process.env.ENVIRONMENT && process.env.ENVIRONMENT === 'development') ?
    app.listen(PORT, () => console.log(`App is listening on port http://${SERVER}:${PORT}`)) :
    app.listen(PORT, () => console.log(`Server was starting, on ${SERVER}:${PORT}`));
