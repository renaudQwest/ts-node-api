import {IUser, User} from "../Models/User.models";

export default class UserControllers {

    public index = () => {
        return User.find({})
            .then( response => response)
            .catch( x => new Error(x) );
    }

    public findOne = (id: string) => {
         return User.findById(id);
    }

    public create = async (body: IUser) => {
        return User.create(body);
    }

    public deleteOne = (id: string) => {
        return User.deleteOne({_id: id})
            .then(response => response)
            .catch( err => new Error(err) );
    }

    public updateOne = (id: string, body: IUser) => {
        return User.findOneAndUpdate({id}, body, { new: true });
    }
}