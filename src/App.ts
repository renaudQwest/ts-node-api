import express from "express";
import * as bodyParser from 'body-parser';
import morgan from "morgan";
import * as dotenv from 'dotenv';
import {parseCliFlag} from './lib/environment';
import path from "path";
import serveStatic from 'serve-static';
import {MainRoutes} from "./Routes/main.routes";
import {UserRoutes} from "./Routes/User.routes";
import mongoose, {Mongoose} from "mongoose";

export class App {
    public app: express.Application = express();
    private environment: string = parseCliFlag('env')!
    private assets: string = path.join(__dirname,  "/assets/");

    //Connection database
    private databaseUrl: string = 'mongodb://127.0.0.1:27017/test'

    // Routes
    private routesPrv: MainRoutes = new MainRoutes();
    private userRoutes: UserRoutes = new UserRoutes();

    public constructor() {
        this.config();
        this.logger();
        this.assetsLocation();
        this.routes()
        this.databaseSetup();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
        dotenv.config({
            path: path.join(__dirname, `./env/.env.${(this.environment)}` ),
            debug: (process.env.ENVIRONMENT === 'development')
        });
        this.app.use(express.json());

    }

    private logger(): void {
        this.app.use(morgan((process.env.LOGGER) ? process.env.LOGGER : 'dev'));
    }

    private assetsLocation(): void {
        this.app.use(serveStatic(this.assets, {
            maxAge: '15d'
        }));
    }

    private routes(): void {
        this.routesPrv.path(this.app);
        this.userRoutes.path(this.app);
    }

    private databaseSetup(): void {
        mongoose.connect(
            this.databaseUrl,
            { useNewUrlParser: true }
        ).then( (param: Mongoose) => {
            return console.log(`Successfully connected to ${this.databaseUrl}`)
        }).catch( error => {
           console.log(`Error connecting to database: `, error);
           return process.exit(1);
        });
    }
}

export default new App().app;