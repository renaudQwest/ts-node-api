import mongoose, {model, Schema} from "mongoose";

const CompanySchema = new Schema({
    name: {
        type: String,
        required: [true, "Company name is required"]
    }
});

export const company = model("Company", CompanySchema);