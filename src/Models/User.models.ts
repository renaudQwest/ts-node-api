import {model, Schema} from "mongoose";

export interface IUser {
    firstName: string,
    lastName?: string,
    username: string,
    password: string,
    gender: Gender;
    friends?: Array<string>;
    creditCards?: Map<string, string>;
}

export enum Gender {
    Female,
    Male
}

const UserSchemaFields: Record<keyof IUser, any> = {
    firstName: {
        type: String,
        required: true
    },
    lastName: String,
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    gender: {
        type: Number,
        enum: [0, 1],
        default: 0,
        required: true
    },
    friends: [{
        type: String,
    }],
    creditCards: {
        type: Map,
        of: String
    }
}

const UserSchema: Schema<IUser> = new Schema(UserSchemaFields);

export const User = model("user", UserSchema, "test")