import UserControllers from "../Controllers/User.controlllers";
import {Application, Request, Response} from "express";
import {IUser} from "../Models/User.models";

export class UserRoutes {

    private UserController: UserControllers = new UserControllers();
    private statusCode: number = 200;
    private messageStatus: string = 'Ok';
    private currentPath: string = "/user"

    public path(app: Application): void {

        app.route(this.currentPath)
            .get(async (req: Request, res: Response) => {
                try {
                    const data = await this.UserController.index();

                    return await res.status(200).json({
                        code: this.statusCode,
                        message: this.messageStatus,
                        data: data
                    })
                } catch (e) {
                    console.log(e);
                    return res.status(500)
                }
            });

        app.route(`${this.currentPath}/find/:id`)
            .get(async (req: Request, res: Response,) => {
                try {
                    const ID = req.params['id'];
                    const data = await this.UserController.findOne(ID);

                    console.log(await data);

                    return await res.status(200).json({
                        code: 200,
                        message: `User find with id: ${ID}`,
                        data: data
                    });
                } catch (e) {
                }
            })

        app.route(`${this.currentPath}/add`)
            .post(async (req: Request, res: Response) => {
                try {
                    const data = await this.UserController.create(req.body);

                    res.status(200).json({
                        code: 200,
                        message: "Value validated",
                        data: data
                    });
                } catch (e) {
                    const ERRORS = e.message.split(",");

                    res.status(400).json({
                        code: 400,
                        message: ERRORS
                    });
                }
            });

        app.route(`${this.currentPath}/update/:id`)
            .put(async (req: Request, res: Response) => {
                let status = 200
                try {
                    const ID = req.params['id'];
                    const body: IUser = req.body;
                    let data: any = undefined;

                    if (UserRoutes.isInstance(data)) {
                        data = this.UserController.updateOne(ID, req.body);
                    } else {
                        status = 400
                        throw new Error(`Your body is wrong`)
                    }

                    if (data) {
                        return res.status(status).json({
                            code: 200,
                            message: `Id ${ID}, ${body.firstName} ${body.lastName} (${body.username}) was modified with :
                            ${data}`
                        })
                    } else {
                        status = 404
                        throw new Error(`Id ${ID} don't exist`);
                    }

                } catch (e) {
                    const idError = e.messageId,
                          messageText = e.messageText;

                    res.status(status).json({
                        code: (status) ? status : 400,
                        message: (messageText) ? idError + ':' + messageText : e.message
                    })
                }
            });

        app.route(`${this.currentPath}/delete/:id`)
            .delete(async (req: Request, res: Response) => {
                try {

                    const ID = req.params['id'];
                    const data = await this.UserController.deleteOne(ID);

                    if (data) {
                        res.status(200).json({
                            code: 200,
                            message: `The user with ${ID} was been deleted`
                        });
                    } else {
                        throw new Error(`Id ${ID} don't exist`);
                    }

                } catch (e) {
                    const ERRORS = e.message

                    res.status(404).json({
                        code: 404,
                        message: ERRORS
                    });
                }
            });
    }

    private static isInstance(object: any): IUser {
        return object as IUser
    }


}