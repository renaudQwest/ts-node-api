import express, {Request, Response} from 'express';
import MainControllers from "../Controllers/main.controlllers";

export class MainRoutes {

    private MainController: MainControllers = new MainControllers();
    private statusCode: number = 0;
    private messageStatus: string = '';

    public path(app: express.Application): void {

        app.route('/')
            .get((req: Request, res: Response): Response<any> => {
                    try {
                        return res.status(404).json({message: this.MainController.index('Renaud')});
                    } catch (e) {
                        return res.status(e.response.status).json(MainRoutes.handlerError(this.statusCode, this.messageStatus));
                    }
                }
            );
    }

    private static handlerError(code: number,message: string) {
        if (code) {
            return {
                code: code,
                message: message,
            };
        }

        return {
            code: 'unknown',
            message: 'Error unknown'
        }
    }
}